FROM openjdk:8-jdk-alpine
WORKDIR /app
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
EXPOSE 6060
ENTRYPOINT ["java", "-Dspring.profiles.active=deploy", "-jar", "app.jar"]
