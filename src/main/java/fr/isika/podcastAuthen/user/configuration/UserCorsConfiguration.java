package fr.isika.podcastAuthen.user.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class UserCorsConfiguration {
	@Bean
	public CorsFilter corsFilter() {

		CorsConfiguration config = new CorsConfiguration();

		config.addAllowedOrigin("*");

		config.setAllowCredentials(true);

		config.addAllowedMethod("*");

		config.addAllowedHeader("*");

		UrlBasedCorsConfigurationSource corsConfigurationSource = new UrlBasedCorsConfigurationSource();
		corsConfigurationSource.registerCorsConfiguration("/**", config);

		return new CorsFilter(corsConfigurationSource);

	}

}
