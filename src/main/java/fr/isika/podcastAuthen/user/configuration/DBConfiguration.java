package fr.isika.podcastAuthen.user.configuration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ConfigurationProperties("spring.datasource")
public class DBConfiguration {
	

	
	private String driverClassName;
	private String url;
	private String username;
	private String password;

	public String getDriverClassName() {
		return driverClassName;
	}

	public void setDriverClassName(String driverClassName) {
		this.driverClassName = driverClassName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Profile("dev")
	@Bean
	public String devDatabaseConnection() {
		System.out.println("DB connection for DEV - CONTAINER");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB connection for DEV - CONTAINER";
	}

	@Profile("prod")
	@Bean
	public String prodDatabaseConnection() {
		System.out.println("DB Connection to mysql - local mysql");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB Connection to mysql - local mysql";
	}
	
	@Profile("deploy")
	@Bean
	public String deployDatabaseConnection() {
		System.out.println("DB Connection to MYSQLHEROKU - CLEAN mysql");
		System.out.println(username);
		System.out.println(password);
		System.out.println(url);
		return "DB Connection to MYSQLHEROKU - CLEAN mysql";
	}
	
//	@Profile("test")
//	@Bean
//	public String testDatabaseConnection() throws SQLException {
//	
//		System.out.println("DB Connection to MYSQLTEST - TEST mysql");
//		System.out.println(driverClassName);
//		System.out.println(username);
//		System.out.println(password);
//		System.out.println(url);
//		return "DB Connection to MYSQLtest - test mysql";
//
//	}
	
	
	
	
	


}
