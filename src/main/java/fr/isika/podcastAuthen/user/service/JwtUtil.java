package fr.isika.podcastAuthen.user.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import fr.isika.podcastAuthen.user.entity.MyUserDetails;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtUtil {
	 private static String SECRET_KEY = "secret";

	    public String extractUsername(String token) {
	        return extractClaim(token, Claims::getSubject);
	    }
	    
	    public String extractEmail(String token) {
	        return extractClaim(token, Claims::getSubject);
	    }

	    public Date extractExpiration(String token) {
	        return extractClaim(token, Claims::getExpiration);
	    }

	    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
	        final Claims claims = extractAllClaims(token);
	        return claimsResolver.apply(claims);
	    }
	    private Claims extractAllClaims(String token) {
	        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
	    }

	    private Boolean isTokenExpired(String token) {
	        return extractExpiration(token).before(new Date());
	    }

	    public String generateToken(UserDetails myUserDetails) {
	        Map<String, Object> claims = new HashMap<>();
	        return createToken(claims, ((MyUserDetails) myUserDetails).getEmail());
	    }

	    private String createToken(Map<String, Object> claims, String subject) {

	        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
	                .setExpiration(new Date(System.currentTimeMillis() + (60000*60) ))
	                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	    }

	    public Boolean validateToken(String token, MyUserDetails myUserDetails) {
	        final String email = extractEmail(token);
	        return (email.equals(myUserDetails.getEmail()) && !isTokenExpired(token));
	    }
	    
	    public String getUserIdByJwtToken(HttpServletRequest request) {
	        String jwtToken = request.getHeader("Authorization").substring(7);
	     
	        if(StringUtils.isEmpty(jwtToken)) return "";
	        Jws<Claims> claimsJws = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(jwtToken);
	        System.out.println("*********************"+claimsJws);
	        Claims claims = claimsJws.getBody();
	        return (String) claims.get("sub");
	    }


}
