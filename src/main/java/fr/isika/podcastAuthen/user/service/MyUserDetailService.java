package fr.isika.podcastAuthen.user.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.isika.podcastAuthen.user.entity.MyUserDetails;
import fr.isika.podcastAuthen.user.entity.User;
import fr.isika.podcastAuthen.user.repository.UserRepository;

@Service
public class MyUserDetailService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("no user with " + email));
		MyUserDetails myUserDetails = new MyUserDetails();
		myUserDetails.setEmail(user.get().getEmail());
		myUserDetails.setPassword(user.get().getPwd());
		return myUserDetails;

	}

}
