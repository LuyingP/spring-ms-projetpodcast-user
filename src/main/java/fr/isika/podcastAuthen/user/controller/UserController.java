package fr.isika.podcastAuthen.user.controller;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.isika.podcastAuthen.user.entity.AuthenticationRequest;
import fr.isika.podcastAuthen.user.entity.AuthenticationResponse;
import fr.isika.podcastAuthen.user.entity.MyUserDetails;
import fr.isika.podcastAuthen.user.entity.User;
import fr.isika.podcastAuthen.user.repository.UserRepository;
import fr.isika.podcastAuthen.user.service.JwtUtil;
import fr.isika.podcastAuthen.user.service.MyUserDetailService;

@RestController
@CrossOrigin("https://angular-podcast.herokuapp.com/")
public class UserController {
//	@Value("${app.message}")
//	private String welcomeMessage;
//	@GetMapping("/welcome")
//	public String getDataBaseConnectionDetails() {
//		return welcomeMessage;
//	}

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private JwtUtil jwtUtil;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private MyUserDetailService myUserDetailService;

	private static final Logger log = LoggerFactory.getLogger(UserController.class);

	@PostMapping("/authentication")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {
         if(userRepository.findByEmail(authenticationRequest.getEmail()).isPresent()) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(),
					authenticationRequest.getPwd()));
		} catch (BadCredentialsException e) {
		System.out.println("Incroct email or password");
			throw new Exception("Incroct email or password", e);
		}
		MyUserDetails myUserDetails = (MyUserDetails) myUserDetailService
				.loadUserByUsername(authenticationRequest.getEmail());
		final String JWTTOEN = jwtUtil.generateToken(myUserDetails);
		return ResponseEntity.ok(new AuthenticationResponse(JWTTOEN));
		}else {
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping("/register")
	public ResponseEntity<?> createOneUtilisateur(@Valid @RequestBody User utilisateur) {
		User utilisateurAjout = new User();
		Optional<User> utilisateurExistant = userRepository.findByEmail(utilisateur.getEmail());
		if (utilisateurExistant.isPresent()) {
			System.out.println("@@@@@@@@@ null");
			return ResponseEntity.badRequest().build();
		} else {
			utilisateurAjout = this.userRepository.save(utilisateur);
			System.out.println("~~~~~~~~~~~~~~~~~~~~ ajout un user "+utilisateur.getUsername());
//			return ResponseEntity.created(location)
			return ResponseEntity.ok(utilisateurAjout);
		}
	}


	@GetMapping("/userinfo")
	public User getUserInfo(HttpServletRequest request) {
		System.out.println("retour token ///////"+request);
		String email = jwtUtil.getUserIdByJwtToken(request);
		Optional<User> userinfo = userRepository.findByEmail(email);
		if (userinfo.isPresent()) {
			return userinfo.get();
		} else {
			return null;
		}
	}


}
