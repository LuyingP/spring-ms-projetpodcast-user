package fr.isika.podcastAuthen.user.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import javax.validation.constraints.NotNull;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer id;

	@NotNull(message = "username cannot be null")
	private String username;

	@NotNull(message = "email cannot be null")
	@Email(message = "email format")
	private String email;

	@NotBlank(message = "Password is required")
	@Size(min = 8, message = "Password should have min 8 characters")
	private String pwd;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public User(Integer id, @NotNull(message = "username cannot be null") String username,
			@NotNull(message = "email cannot be null") @Email(message = "email format") String email,
			@NotBlank(message = "Password is required") @Size(min = 8, message = "Password should have min 6 characters") String pwd) {
		super();
		this.id = id;
		this.username = username;
		this.email = email;
		this.pwd = pwd;
	}
	
	

	public User(@NotNull(message = "username cannot be null") String username,
			@NotNull(message = "email cannot be null") @Email(message = "email format") String email,
			@NotBlank(message = "Password is required") @Size(min = 8, message = "Password should have min 8 characters") String pwd) {
		super();
		this.username = username;
		this.email = email;
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [id=");
		builder.append(id);
		builder.append(", username=");
		builder.append(username);
		builder.append(", email=");
		builder.append(email);
		builder.append(", pwd=");
		builder.append(pwd);
		builder.append(", role=");
		builder.append("]");
		return builder.toString();
	}

	public User() {

	}

}
