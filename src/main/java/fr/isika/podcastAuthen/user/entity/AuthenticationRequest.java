package fr.isika.podcastAuthen.user.entity;

import java.io.Serializable;

public class AuthenticationRequest implements Serializable{
	
	private String email;
	private String pwd;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public AuthenticationRequest(String email, String pwd) {
		super();
		this.email = email;
		this.pwd = pwd;
	}
	public AuthenticationRequest() {
	}
	
	

}
