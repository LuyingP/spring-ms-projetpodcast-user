//  package fr.isika.podcastAuthen.user;

//  import org.springframework.beans.factory.annotation.Autowired;
//  import org.springframework.boot.ApplicationArguments;
//  import org.springframework.boot.ApplicationRunner;
//  import org.springframework.stereotype.Component;

//  import fr.isika.podcastAuthen.user.entity.User;
//  import fr.isika.podcastAuthen.user.repository.UserRepository;

//  @Component
//  public class UserInitialisation implements ApplicationRunner {
//  	@Autowired
//  	private UserRepository userRepository;

//  	@Override
//  	public void run(ApplicationArguments args) throws Exception {
//  		User u1 = new User();
//  		u1.setUsername("zoigo");
//  		u1.setEmail("paul.dupetit@gmail.com");
//  		u1.setPwd("Wj123456@");

//  		User u2 = new User();
//  		u2.setUsername("eva");
//  		u2.setEmail("eva.azeo@gmail.com");
//  		u2.setPwd("Wj123456@");

//  		this.userRepository.save(u1);
//  		this.userRepository.save(u2);
//  	}
//  }
