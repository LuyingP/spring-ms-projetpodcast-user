package fr.isika.podcastAuthen.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
@EntityScan("fr.isika.podcastAuthen.user.entity")
@SpringBootApplication(scanBasePackages = {"package fr.isika.podcastAuthen.user"})
public class UserApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserApplication.class, args);
	}

}
