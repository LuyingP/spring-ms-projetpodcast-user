package fr.isika.podcastAuthen.user.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import fr.isika.podcastAuthen.user.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	public Optional<User> findByEmail(String email);

	public List<User> findAll();

}
