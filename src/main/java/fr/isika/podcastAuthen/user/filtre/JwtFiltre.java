package fr.isika.podcastAuthen.user.filtre;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import fr.isika.podcastAuthen.user.entity.MyUserDetails;
import fr.isika.podcastAuthen.user.service.JwtUtil;
import fr.isika.podcastAuthen.user.service.MyUserDetailService;

@Component
public class JwtFiltre extends OncePerRequestFilter {

	@Autowired
	private MyUserDetailService myUserDetailService;

	@Autowired
	private JwtUtil jwtUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		final String authorizationHeader = request.getHeader("Authorization");

		String email = null;
		String jwt = null;

		if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
			jwt = authorizationHeader.substring(7);
			email = jwtUtil.extractEmail(jwt);
		}

		if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {

			MyUserDetails myUserDetails = (MyUserDetails) this.myUserDetailService.loadUserByUsername(email);

			if (jwtUtil.validateToken(jwt, myUserDetails)) {

				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						myUserDetails.getEmail(), myUserDetails.getPassword());
				usernamePasswordAuthenticationToken
						.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			}
		}
		chain.doFilter(request, response);
	}

}
