package fr.isika.podcastAuthen.user;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import fr.isika.podcastAuthen.user.UserApplication;
import fr.isika.podcastAuthen.user.entity.User;
import fr.isika.podcastAuthen.user.repository.UserRepository;

@ActiveProfiles("test")
@ContextConfiguration
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = { UserApplication.class,
		testProfileJpaConfig.class })
public class UserTestTemplateTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private UserRepository userRepository;

	@LocalServerPort
	int randomServerPort;

	@Test
	public void testAddUserSuccess() throws URISyntaxException {
		userRepository.deleteAll();
		final String baseUrl = "http://localhost:" + randomServerPort + "/register";
		URI uri = new URI(baseUrl);
		User u1 = new User();
		u1.setUsername("jacky");
		u1.setPwd("Wj123456@");
		u1.setEmail("jacky@gmail.com");

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");

		HttpEntity<User> request = new HttpEntity<>(u1, headers);

		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);

		Assert.assertEquals(200, result.getStatusCodeValue());
	}

	@Test
	public void testAddUserFailure() throws URISyntaxException {
		
		userRepository.deleteAll();
		final String baseUrl = "http://localhost:" + randomServerPort + "/register";
		URI uri = new URI(baseUrl);
		User u1 = new User();
		u1.setUsername("jacky");
		u1.setPwd("Wj123456@");
		u1.setEmail("jacky@gmail.com");

		HttpHeaders headers = new HttpHeaders();
		headers.set("X-COM-PERSIST", "true");

		HttpEntity<User> request = new HttpEntity<>(u1, headers);

		ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);

		Assert.assertEquals(200, result.getStatusCodeValue());

		final String baseUrl2 = "http://localhost:" + randomServerPort + "/register";
		URI uri2 = new URI(baseUrl2);
		User u2 = new User();
		u2.setUsername("jacky");
		u2.setPwd("Wj123456@");
		u2.setEmail("jacky@gmail.com");

		HttpHeaders headers2 = new HttpHeaders();
		headers2.set("X-COM-PERSIST", "true");

		HttpEntity<User> request2 = new HttpEntity<>(u2, headers2);

		ResponseEntity<String> result2 = this.restTemplate.postForEntity(uri2, request2, String.class);

		Assert.assertEquals(400, result2.getStatusCodeValue());
		

	}
  


}
