package fr.isika.podcastAuthen.user;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParseException;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.isika.podcastAuthen.user.entity.MyUserDetails;
import fr.isika.podcastAuthen.user.entity.User;
import fr.isika.podcastAuthen.user.repository.UserRepository;
import fr.isika.podcastAuthen.user.service.JwtUtil;
import fr.isika.podcastAuthen.user.service.MyUserDetailService;

@ActiveProfiles("test")
@AutoConfigureMockMvc
@ContextConfiguration
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = { UserApplication.class,
		testProfileJpaConfig.class })
public class UserApplicationTests {

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private JwtUtil jwtUtil;

	@MockBean
	private MyUserDetailService myUserDetailService;

	@MockBean
	MyUserDetails myUserDetail;

	@Autowired
	private MockMvc mvc;

	@Autowired
	private WebApplicationContext context;

	@Before
	public void setup() {
		mvc = MockMvcBuilders.webAppContextSetup(context).apply(springSecurity()).build();
	}

	private String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

	private <T> T mapFromJson(String json, Class<T> clazz)
			throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clazz);
	}

	@Test
	public void creatNewUser() throws Exception {
		User u1 = new User("abc", "123Abc@A", "abcd@gmail.com");
		String inputJson = mapToJson(u1);

		String uri = "/register";
		MvcResult mvcResult = mvc.perform(
				MockMvcRequestBuilders.post(uri, u1).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
				.andReturn();

		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

	}

	@Test
	public void shouldNotAllowAccessToUnauthenticatedUsers() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post("/authentication"))
				.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

//	 @Test
//     public void shouldGenerateAuthToken() throws Exception {
//		 User u1=new User("zoe","zoe@gmail.com","Wj123456@");
//		 when(userRepository.save(any(User.class))).thenReturn(u1);
//		 AuthenticationRequest r1=new AuthenticationRequest(u1.getEmail(),u1.getPwd());
//		 String inputJson = mapToJson(r1);

//		 MvcResult mvcResult = mvc.perform(
//					MockMvcRequestBuilders.post("/authentication")
//					.with(csrf())
//					.contentType(MediaType.APPLICATION_JSON_VALUE)
//					.content(inputJson))
//					.andReturn();
//		
//		 verify(myUserDetailService).loadUserByUsername(u1.getEmail());

//		 MockMvcRequestBuilders.post("/authentication")
//			.with(user(myUserDetail));
//     }

//	 @Test
//     public void testGetUserInforFromToken() throws Exception {
//		 MvcResult mvcResult = mvc.perform(
//				 MockMvcRequestBuilders.get("/userinfo")
//				 .with(csrf())
//				 .contentType(MediaType.APPLICATION_JSON_VALUE))
//				 .andReturn();
//         verify(Optional.of(userRepository.findByEmail("abc@abc.com")));
//     }

	
}
