package fr.isika.podcastAuthen.user;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableJpaRepositories(basePackages = "fr.isika.podcastAuthen.user.repository")
@EnableTransactionManagement
@Configuration
@Profile("test")
public class testProfileJpaConfig {
	@Autowired
    private Environment env;

    @Bean
    @Profile("test")
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();

        
//       dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");        
//        dataSource.setUrl("jdbc:mysql://localhost:3399/podcast_user_test?serverTimezone=Europe/Paris&zeroDateTimeBehavior=CONVERT_TO_NULL");
//        dataSource.setUsername("root");
//        dataSource.setPassword("isika");
        
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:testdbuser;DB_CLOSE_DELAY=-1");
        dataSource.setUsername("sa");
        dataSource.setPassword("sa");

        return dataSource;
    }
	
	 @Bean
	    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
	        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
	        em.setDataSource(dataSource());
	        em.setPackagesToScan(new String[] { "fr.isika.podcastAuthen.user.entity" });
	        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
	        em.setJpaProperties(additionalProperties());
	        return em;
	    }

	    @Bean
	    JpaTransactionManager transactionManager(final EntityManagerFactory entityManagerFactory) {
	        final JpaTransactionManager transactionManager = new JpaTransactionManager();
	        transactionManager.setEntityManagerFactory(entityManagerFactory);
	        return transactionManager;
	    }
	
	    final Properties additionalProperties() {
	        final Properties hibernateProperties = new Properties();

	        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
	        hibernateProperties.setProperty("hibernate.dialect", env.getProperty("hibernate.dialect"));
	        hibernateProperties.setProperty("hibernate.show_sql", env.getProperty("hibernate.show_sql"));

	        return hibernateProperties;
	    }

}
